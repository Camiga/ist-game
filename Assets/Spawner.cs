﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Meteor spawners.
    public Transform[] spawnPoints;
    public GameObject[] hazards;

    // Meteor timers.
    private double timeBtwSpawns;
    public double StartTimeBtwSpawns;
    public double minTimeBetweenSpawns;
    public double decrease;

    // Player to targer.
    public GameObject player;

    // Update is called once per frame.
    private void Update()
    {
        if (player == null)
        {
            return;
        }
        if (timeBtwSpawns > 0)
        {
            timeBtwSpawns -= Time.deltaTime;
        }
        else
        {
            // Spawn objects.
            Transform randomSpawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
            GameObject randomHazard = hazards[Random.Range(0, hazards.Length)];

            // Spawns random hazard at random spawn point.
            _ = Instantiate(randomHazard, randomSpawnPoint.position, Quaternion.identity);

            if (StartTimeBtwSpawns > minTimeBetweenSpawns)
            {
                StartTimeBtwSpawns -= decrease;
            }

            // Reset counter back to the start value.
            timeBtwSpawns = StartTimeBtwSpawns;
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    // "Game over" screen.
    public GameObject lostPanel;

    // Movement.
    public int speed;
    private float input;
    private Rigidbody2D rb;

    // Damage sound effects.
    private AudioSource source;

    // Animation.
    private Animator anim;

    // Set up health system.
    public float health;
    public Text healthDisplay;

    // Dash ability setup.
    public int extraSpeed;
    public float startDashTime;
    private float dashTime;
    private bool isDashing;

    // Set up walls to stop player from leaving the scene.
    // Create a bool to help suspend the movement of the character.
    private bool CanMove;
    // This integer is the opposite direction of the wall. This is used to allow the player to back away from the wall.
    private int InputToWatch;

    // Start is called before the first frame update. Use this for initialization.
    private void Start()
    {
        // Animation.
        anim = GetComponent<Animator>();
        // Movement.
        rb = GetComponent<Rigidbody2D>();
        // Health HUD.
        healthDisplay.text = health.ToString();
        // Damage sound effect.
        source = GetComponent<AudioSource>();
        // Allow the player to move at the beginning.
        CanMove = true;
    }

    // Update is called once per frame.
    private void Update()
    {
        // Set running boolean.
        if (input == 0)
        {
            anim.SetBool("isRunning", false);
        }
        else
        {
            anim.SetBool("isRunning", true);
        }

        // Flip character around depending on the input.
        if (input > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (input < 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        // Allow player to dash.
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.LeftShift)) && !isDashing)
        {
            speed += extraSpeed;
            isDashing = true;
            dashTime = startDashTime;
        }
        if (dashTime <= 0 && isDashing == true)
        {
            isDashing = false;
            speed -= extraSpeed;
        }
        else
        {
            dashTime -= Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {
        // Update "input" with horizontal movement if the player is allowed to move. Otherwise, set the input to 0.
        input = CanMove == true ? Input.GetAxisRaw("Horizontal") : 0;

        // Let the player move if they are trying to get away from the wall while their input is suspended.
        if (input == 0 && Input.GetAxisRaw("Horizontal") == InputToWatch)
        {
            CanMove = true;
        }

        // Move the player.
        rb.velocity = new Vector2(input * speed, rb.velocity.y);
    }

    // Function for player to take damage. This function is called by the enemy script.
    public void TakeDamage(int damageAmount)
    {
        // Play damage sound effect.
        source.Play();
        // Play animation when hurt.
        anim.SetTrigger("Hurt");
        // Remove health when hit.
        health -= damageAmount;
        // Set health display text.
        healthDisplay.text = health.ToString();
        /* The following line of code dynamically changes the color of the health display.
         * The less health you have, the brighter red the health display will be.
         * This works by diving the health value by 100 to get a decimal, then subtracting it from one.
         * Doing so gives a decimal that we can use as a float to change the intensity of red shown on the health display.
         */
        healthDisplay.color = new Color(1 - health / 100 / 1f, 0, 0, 1);
        if (health <= 0)
        {
            // Kill the player.
            Destroy(gameObject);
            // Reset health to 0 to prevent negatives from showing on the healthDisplay.
            health = 0;
            healthDisplay.text = health.ToString();
            // Show game over screen.
            lostPanel.SetActive(true);
        }
    }

    public void PauseInput(int InputToSuspend)
    {
        // Check if the player is trying to get past the wall, by checking if their input is illegal.
        if (input == InputToSuspend)
        {
            // Stop the player from moving.
            CanMove = false;
            // Set the InputToWatch to the exact opposite of the wall. This allows the player to back away.
            InputToWatch = InputToSuspend * -1;
        }
    }
}

﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Various speed values.
    public int minSpeed;
    public int maxSpeed;
    private int speed;

    // Damage system.
    private Player playerScript;
    public int damage;

    // Explosion particle effects.
    public GameObject explosion;

    // Start is called before the first frame update.
    // Use this for initialization.
    private void Start()
    {
        speed = Random.Range(minSpeed, maxSpeed);
        playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    private void Update() => transform.Translate(Vector2.down * speed * Time.deltaTime);

    private void OnTriggerEnter2D(Collider2D hitObject)
    {
        // Destroy the meteor and create particles if it hits something.
        if (hitObject)
        {
            Destroy(gameObject);
            _ = Instantiate(explosion, transform.position, Quaternion.identity);
        }
        // Deal damage to the player if they were hit.
        if (hitObject.tag == "Player")
        {
            playerScript.TakeDamage(damage);
        }
    }
}

﻿using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    private float deltaTime;
    private readonly float colorValue = 153 / 255f;

    // Start is called before the first frame update.
    private void Start()
    {
        // Create a repeater that runs every 0.5 seconds, which can be used in void EverySecond()
        InvokeRepeating("UpdateClock", 0.5f, 0.5f);
        // Catch FPS for the first frame.
        deltaTime += Time.unscaledDeltaTime - deltaTime;
    }

    // Update FPS counter every half a second.
    private void UpdateClock() => deltaTime += Time.unscaledDeltaTime - deltaTime;

    // When game is started.
    private void OnGUI()
    {
        GUIStyle style = new GUIStyle
        {
            // Place counter in the top right corner of the screen.
            alignment = TextAnchor.UpperLeft,
            // Set font size.
            fontSize = Screen.height * 4 / 100
        };
        // Change text color.
        style.normal.textColor = new Color(colorValue, colorValue, colorValue, 1);

        // Draw a rectangle the size of the screen to house the FPS counter in.
        // Draw FPS counter in the rectangle, with numbers for fps and ms, using the style guide declared above.
        GUI.Label(new Rect(0, 0, Screen.width, Screen.height * 5 / 100),
                  string.Format("{1:0.} fps ({0:0.00} ms)", deltaTime * 1000, 1 / deltaTime),
                  style);
    }
}

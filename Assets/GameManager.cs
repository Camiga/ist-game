﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Scene manager for menu.
    private void PlayGame() => SceneManager.LoadScene("Game");
}

﻿using UnityEngine;

public class WallScript : MonoBehaviour
{
    // Initiate a variable for the player.
    private Player playerScript;

    // Direction to suspend, which is set by the wall.
    public int Direction;

    // Find the player.
    private void Start() => playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

    // When something hits the wall.
    private void OnTriggerEnter2D(Collider2D hitObject)
    {
        // Check if it's the player first.
        if (hitObject.tag == "Player")
        {
            // Run the script in Player.cs to kill the players input, using the direction of the wall.
            playerScript.PauseInput(Direction);
        }
    }
}
